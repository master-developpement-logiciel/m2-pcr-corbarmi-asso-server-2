package m2dl.corbarmi;

import java.util.HashMap;
import java.util.Map;

public class ServerImpl {

	public ServerImpl() {
		map.put("AS1", new Asso("Asso1", "L'asso no1", "31400"));
		map.put("AS2", new Asso("Asso2", "L'asso no2", "31100"));
		map.put("AS3", new Asso("Asso3", "L'asso no3", "75011"));
		map.put("AS4", new Asso("Asso4", "L'asso no4", "64200"));
		map.put("AS5", new Asso("Asso5", "L'asso no5", "31000"));
		map.put("AS6", new Asso("Asso6", "L'asso no6", "94130"));
	}

	private final Map<String, Asso> map = new HashMap<String, Asso>();

	public String getAssoCP(String assoID) {
		return map.get(assoID).cp;
	}

	public String getAssoName(String assoID) {
		return map.get(assoID).name;
	}

	public String[] getAssoIDs() {
		return map.keySet().toArray(new String[]{});
	}

	public String getDescription(String assoID) {
		return map.get(assoID).desc;
	}
	
	public static void main(String args[]) {
		// TODO completer la classe ServerImpl pour en faire l'implementation d'un objet distant
		// TODO creer l'annuaire
		// TODO creer le serveur et l'enregistrer dans l'annuaire sous le nom "AssoServer"
	}

}
